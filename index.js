// SOAL NO 1
//Buatlah sebuah function dengan nama changeWord yang berfungsi untuk
//menggantikan sebuah kata didalam sebuah kalimat.
function changeWord (selectedText, changedText, text){
  return text.replace(selectedText, changedText)
}

const sentence1 = 'Andi sangat mencintai kamu selamanya'
const sentence2 = 'Gunung merapi tak akan mampu menggambarkan besarnya cintaku padamu'

console.log(changeWord('mencintai','menyayangi',sentence1))
  
console.log (changeWord('merapi','semeru',sentence2))


              

// SOAL NO 2
//Buatlah sebuah function yang berfungsi mendeteksi apakah sebuah angka termasuk angka
//genap atau ganjil.

function genapGanjil(bil){
  if(typeof(bil) == "number"){
    if(bil % 2 ==0){
      console.log('BIlangan genap')
    }else{
      console.log('BIlangan ganjil') 
    }
    
  }else{
    console.log ('Bukan bilangan')
  }
  
}
genapGanjil(10)
genapGanjil(3)
//genapGanjil("3")) //eror
genapGanjil({})
genapGanjil([])
genapGanjil()


// SOAL NO 3
//Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah
//alamat email yang diberikan sebagai parameter, adalah alamat email yang formatnya
//benar atau tidak.
function checkEmail(email ){
  var validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if  (typeof email === "string" && email.includes("@")){
    if (email.match(validation)){
       return ('valid');
       }
    else
    {return ('INVALID')}
  }else{
    return Error("Invalid data type")
  }
}
console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata'))

// SOAL NO 4
//Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah password yang diberikan sebagai parameter
//memenuhi kreteria yang telah ditentukan atau tidak.

function isValidPassword (email) {
  const regex = /(?=,*[a-z])(?=,*[0-9])(?=,*[A-Z])/;
  if (typeof givenPassword === 'string') {
    if (regex.test(givenPassword) && givenPassword.length >= 8) {
      return true;
    } else {
      return false;
    }
  } else if (typeof givenPassword === 'undefined') {
    return 'ERROR : Error';
  } else {
    return 'ERROR : invalif data type';
  }
}

console.log(isValidPassword('Meong2021'));
console.log(isValidPassword('meong2021'));
console.log(isValidPassword('@eong'));



 
//Buatlah sebuah function yang berfungsi untuk membagikan sebuah nama menjadi Nama Depan, Nama Tengah, Nama Belakang.
//Function ini nantinya akan menerima satu parameter yang berisi nama lengkap seseorang. Apabila nama lengkap dari seseorang
//tersebut lebih dari 3 suku kata, maka function tersebut harus menghasilkan sebuah error. Tapi apabila parameter yang diberikan
//valid (tidak lebih dari 3 suku kata), maka function ini akan menghasilkan sebuah object dengan properti firstName, middleName,
//lastName.

const getSplitName = (personName) => {
    if(typeof personName === "string"){
        personName = personName.split(" ")
        if(personName.length == 3){
          return ("firstName: " + personName[0] + " , " + "middleName: " + personName[1] + " , " + "lastName: " + personName[2])
        } else if (personName.length == 2){
          return ("firstName: " + personName[0] + " , " + "middleName: " + personName[1] + " , " + "lastName: " + null)
        } else if (personName.length == 1){
          return ("firstName: " + personName[0] + " , " + "middleName: " + null + " , " + "lastName: " + null)
        } else {
          return Error("Invalid Data Type")
        }
    }
}

console.log(getSplitName("Aldi Daniela Pranata"))
console.log(getSplitName("Aldi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName(0))


//Buatlah sebuah function yang berfungsi untuk mendapatkan angka terbesar kedua dari sebuah array.
//Misal diberikan sebuah array yang terdiri dari beberapa angka [2,3,5,6,6,4], berdasarkan data dari array tersebut dapat kita
//simpulkan bahwasanya angka terbesar dari array tersebut adalah 6, angka kedua terbesar adalah 5, dan angka ketiga terbesar
//adalah 4. Maka dari itu function yang akan kamu buat ini akan me-return angka kedua terbesar pada array yang telah diberikan,
//yaitu angka 5.

function getAngkaTerbesarKedua(givenNumber){
  if(typeof givenNumber == "object"){
    givenNumber.sort(function(a, b){return b-a})
    return givenNumber[1]
  } else {
    return Error("Invalid data type")
  }
}
  
const dataAngka = [9,4,7,7,4,3,2,2,8]
console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())



//Hari ini Toko Pak Aldi berhasil menjual banyak
//sepatu. Pada gambar disamping terdapat data
//sepatu-sepatu yang terjual dari toko Pak Aldi
//dalam bentuk array of object.
//Tugas kamu adalah membuat sebuah function
//yang berfungsi membantu Pak Aldi untuk
//menghitung total seluruh sepatu yang terjual.

const dataPenjualanPakAldi = [
{
    namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
    hargaSatuan : 760000,
    kategori : "Sepatu Sport",
    totalTerjual : 90,
},
{
    namaProduct : 'Sepatu Warrior Tristan Black Brown High',
    hargaSatuan : 960000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 37,
},
{
    namaProduct : 'Sepatu Warrior Tristan Maroon High',
    hargaSatuan : 360000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
},
{
    namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
    hargaSatuan : 120000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
}
]
const getTotalPenjualan = dataPenjualan => {
    if (typeof dataPenjualan != 'object') {
        return string = 'Invalid type data'
    }
    else {
        let totalPrice = dataPenjualan.reduce(function (accumulator, item) {
            return accumulator + item.totalTerjual;
          }, 0);
    
        return totalPrice
    }
}

console.log(getTotalPenjualan(dataPenjualanPakAldi))
console.log(getTotalPenjualan())

//Hari ini Toko buku milik Ibu Daniela berhasil menjual banyak
//sekali buku-buku novel. Gambar disamping adalah data
//penjualan buku-buku novel yang dijual di Toko buku milik
//Ibu Daniela, dalam format array of object.
//Tugas kamu adalah membuat sebuah function yang
//berfungsi membantu Ibu Daniela untuk mendapatkan
//informasi berupa Total Keuntungan, Total Modal, Produk
//Buku Terlaris, Penulis Buku Terlaris dan Persentase
//Keuntungan dari data penjualan yang telah disediakan
//diatas. function yang kamu buat ini akan me-return sebuah
//data yang berbentuk sebuah object yang dari beberapa
//properti.

const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idPFieroduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];

const currencyFormatting = formatted => {
    return Intl.NumberFormat("id-ID", {
        style: "currency", currency: "IDR" 
    }).format(formatted)
}

const findMax = arr  =>  {
    let max = Math.max.apply(Math, arr.map(function(o) { return o.totalTerjual; }))
    objIndex = arr.findIndex((obj => obj.totalTerjual == max));
    return objIndex
}

const getInfoPenjualan = dataPenjualan => {
    if (typeof dataPenjualan != 'object') {
        return string = "Invalid data type"
    }
    else {
        let totalKeuntungan = dataPenjualan.reduce(function (untungTemp, item) {
            return untungTemp + ((item.hargaJual * item.totalTerjual - item.hargaBeli * (item.totalTerjual + item.sisaStok)));
          }, 0);
        let totalModal = dataPenjualan.reduce(function (modalTemp, item) {
            return modalTemp + (item.hargaBeli * (item.totalTerjual + item.sisaStok));
          }, 0);
        
          
        let persentaseKeuntungan = ((totalKeuntungan / totalModal) * 100).toFixed(0) + "%"
        findMax(dataPenjualan)
          
        return {
            totalKeuntungan: currencyFormatting(totalKeuntungan),
            totalModal: currencyFormatting(totalModal),
            persentaseKeuntungan: persentaseKeuntungan,
            produkBukuTerlaris: dataPenjualan[objIndex].namaProduk,
            penulisTerlaris: dataPenjualan[objIndex].penulis
        }
    }
}

console.log(getInfoPenjualan(dataPenjualanNovel))
console.log(getInfoPenjualan())



